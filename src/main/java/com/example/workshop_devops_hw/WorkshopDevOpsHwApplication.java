package com.example.workshop_devops_hw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkshopDevOpsHwApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkshopDevOpsHwApplication.class, args);
    }

}
