package com.example.workshop_devops_hw.repository;

import com.example.workshop_devops_hw.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {


}
