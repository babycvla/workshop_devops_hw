package com.example.workshop_devops_hw.controllers;

import com.example.workshop_devops_hw.entity.Product;
import com.example.workshop_devops_hw.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @PostMapping(value = "/create-product", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> addProduct(@RequestBody Product product) {
    productRepository.saveAndFlush(product);
    return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/get-products", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getAllProducts(){
    return productRepository.findAll();
    }
}
