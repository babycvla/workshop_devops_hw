FROM openjdk:17
WORKDIR /
COPY ./target/Workshop_DevOps_HW-0.0.1-SNAPSHOT.jar Workshop_DevOps_HW-0.0.1-SNAPSHOT.jar
EXPOSE 8080
CMD java -jar Workshop_DevOps_HW-0.0.1-SNAPSHOT.jar